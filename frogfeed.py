import datetime as dt
import re
import sys
import time
from collections import namedtuple

import bs4
import click
import feedparser
import requests


BOT_NAME = 'Feedbot'


URL_FEED = 'https://www.reddit.com/r/COMPLETEANARCHY/new.rss'
URL_ARCHIVE = 'https://raddle.me/f/COMPLETEANARCHY'
URL_LOGIN = 'https://raddle.me/login'
URL_LOGIN_CHECK = 'https://raddle.me/login_check'
URL_FORUM_SUBMIT = 'https://raddle.me/submit/COMPLETEANARCHY'
URL_SUBMIT = 'https://raddle.me/submit'

RedditPost = namedtuple('Post', 'author link title url date')
RaddlePost = namedtuple('Post', 'author link title')


def parse_reddit_feed(source):
    feed = feedparser.parse(source)
    for entry in feed['entries']:
        author = entry['authors'][0]['name'].split('/')[-1]

        content = entry['content'][0]['value']
        link = re.search(r'<span><a href="(https://.*)">\[link\]', content).group(1)

        title = entry['title']

        url = entry['link']

        date = entry['updated']

        yield RedditPost(author, link, title, url, date)


AUTHOR_SELECTOR = 'p > a'
INFO_SELECTOR = 'h1 > a'

def parse_raddle_bot_posts(source):
    soup = get_soup(source)

    for article in soup('article'):
        author = article.select_one(AUTHOR_SELECTOR).text
        if not author == BOT_NAME:
            continue
        info_tag = article.select_one(INFO_SELECTOR)
        link = info_tag['href']
        title = info_tag.text

        yield RaddlePost(author, link, title)


def get_soup(source):
    return bs4.BeautifulSoup(source, 'html.parser')


def filter_new_posts(archive, feed):
    archive = list(archive)
    for post in feed:
        if not is_new_post(post):
            continue
        if (BOT_NAME, post.link, post.title) in archive:
            print("Skipping:", post)
            print('-'*50)
            continue
        yield post


def is_new_post(post):
    today = dt.date.today()
    post_date = dt.datetime.fromisoformat(post.date)

    return (today.year == post_date.year
            and today.month == post_date.month
            and today.day == post_date.day)


class Submitter:
    """Subclass this to provide network I/O."""
    def __init__(self, username, password, forum):
        self.username = username
        self.password = password
        self.forum = forum
        self._forum_id = None

    def __call__(self, posts):
        print("Logging in..")
        self.login()
        print("Submitting..")
        self.submit(posts)

    def login(self):
        raise NotImplementedError

    @property
    def csrf_token(self):
        return get_csrf_token(self.source)

    @property
    def login_data(self):
        return {'_csrf_token': self.csrf_token,
                '_username': self.username,
                '_password': self.password }

    def make_submission_data(self, post):
        return {
            'submission[url]': post.link,
            'submission[title]': post.title,
            'submission[body]': f'author: {post.author}\n\nurl: {post.url}',
            'submission[forum]': self.forum_id,
            'submission[submit]': None,
            'submission[email]': None,
            'submission[_token]': self.csrf_token
            }

    @property
    def forum_id(self):
        if self._forum_id is None:
            self._forum_id = parse_forum_id(self.forum, self.source)
        return self._forum_id


def get_csrf_token(source):
    return re.search(r'.*_token]?" value="(.*)"', source.decode()).group(1)


def parse_forum_id(forum, source):
    soup = get_soup(source)

    forum_tag_list = soup.select_one('label + select')
    for forum_tag in forum_tag_list('option'):
        if forum_tag.text.lower() == forum.lower():
            return forum_tag['value']


class HttpSubmitter(Submitter):
    """Add http I/O on top of Submitter."""
    def __init__(self, *args, dry_run=False, **kwargs):
        self.dry_run = dry_run
        self.cookies = []
        super().__init__(*args, **kwargs)

    def login(self):
        self.get(URL_LOGIN)
        self.post(URL_LOGIN_CHECK, self.login_data)

    def submit(self, posts):
        for post in posts:
            self.get(URL_FORUM_SUBMIT)
            data = self.make_submission_data(post)
            print('Posting:', post)
            print('-'*50)
            if self.dry_run:
                continue
            self.post(URL_SUBMIT, data)

    def get(self, url):
        response = requests.get(url, cookies=self.cookies, allow_redirects=False)
        self.source = response.content
        if response.cookies:
            self.cookies = response.cookies

    def post(self, url, data):
        response = requests.post(url,
                                 data,
                                 cookies=self.cookies,
                                 allow_redirects=False)

        assert response.status_code == 302
        self.source = response.content
        if response.cookies:
            self.cookies = response.cookies


@click.command()
@click.option('--name', default=None,
              help='The raddle bot account username.')
@click.option('--password', default=None,
              help='The raddle bot account password.')
@click.option('--dry-run', default=False, is_flag=True,
              help='Don not actually post anything, useful for debugging.')
@click.option('--last', default=10, help='Post only the last n posts.')
@click.option('--interval', default=3600, type=click.INT,
              help='Interval at which the bot will look for new posts.')
def main(name, password, dry_run, last, interval):
    if name is None or password is None:
        print("Need to provide username and password.")
        sys.exit(1)


    submitter = HttpSubmitter(name, password, 'completeanarchy', dry_run=dry_run)


    while True:
        posts = get_new_posts()[-last:]
        submitter(posts)
        time.sleep(interval)

def get_new_posts():
    print('Fetching feed', end='')
    feed_response = requests.get(URL_FEED)
    while not feed_response.status_code == 200:
        print('.', end='')
        time.sleep(.5)
        feed_response = requests.get(URL_FEED)
    print()

    new_posts = parse_reddit_feed(feed_response.content)
    new_posts = list(reversed(list(new_posts)))[-15:]

    assert new_posts

    print('Fetching archive..')
    archive_response = requests.get(URL_ARCHIVE)
    archive_posts = parse_raddle_bot_posts(archive_response.content)

    posts = filter_new_posts(archive_posts, new_posts)

    return list(posts)

if __name__ == "__main__":
    main()
