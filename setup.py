from setuptools import setup


setup(
    name='frogfeed',
    version='0.1',
    py_modules=['frogfeed'],
    install_requires=[
        'click'
        ],
    entry_points="""
        [console_scripts]
        frogfeed=frogfeed:main
    """


)
