import datetime
from importlib import resources
from unittest.mock import call

import frogfeed
from frogfeed import *
from tests import data as testdata

import pytest


@pytest.fixture()
def sample():
    with resources.open_text(testdata, 'rss-sample.html') as resource:
        yield resource.read()


def test_parse_reddit_feed(sample):

    result = list(parse_reddit_feed(sample))

    for result, expected in zip(result,[
            ('user_new', 'https://i.redd.it/the_link_new.jpg', 'title_new', 'url_new', '2018-04-01T11:11+00:00'),
            ('user0', 'https://i.redd.it/the_link0.jpg', 'title0', 'url0', '2018-03-01T13:13+00:00'),
            ('user1', 'https://i.redd.it/the_link1.jpg', 'title1', 'url1', '2018-01-01T22:22+00:00'),
            ]):
        assert result == expected


def test_parse_raddle_bot_posts():
    with resources.open_text(testdata, 'raddle-forum-sample.html') as resource:
        result = list(parse_raddle_bot_posts(resource.read()))

    assert result == [
        ('Feedbot', 'link0', 'title0'),
        ('Feedbot', 'link1', 'title1')
        ]


def test_filter_new_posts(mocker):
    stub_date = datetime.date(2018, 3, 2)
    date_mock = mocker.patch('frogfeed.dt.date')
    date_mock.today.return_value = stub_date

    feed_posts = [
            RedditPost('user3', 'link3', 'title3', '', '2018-03-02T22:22+00:00'),
        RedditPost('user2', 'link2', 'title2', '', '2018-03-01T22:22+00:00'),
        RedditPost('user1', 'link1', 'title1', '', '2018-01-01T22:22+00:00'),
        RedditPost('user0', 'link0', 'title0', '', '2018-01-01T22:22+00:00'),
        ]

    archived_posts = [
        RaddlePost('Feedbot', 'link0', 'title0'),
        RaddlePost('Feedbot', 'link1', 'title1')
        ]

    result =  list(filter_new_posts(archived_posts, feed_posts))

    assert result == [
            ('user3', 'link3', 'title3', '', '2018-03-02T22:22+00:00')
            ]


@pytest.fixture
def submitter(mocker):
    class TestSubmitter(Submitter):
        login = mocker.Mock()

    return TestSubmitter('username', 'password', 'forum')


def test_Submitter_call(submitter, mocker):
    submitter.submit = mocker.Mock()

    submitter(['post0', 'post1'])

    assert submitter.login.called
    submitter.submit.assert_called_with(['post0', 'post1'])


def test_Submitter_login_data(submitter):
    submitter.source = b'html stuff [_token]" value="666" more html stuff'

    result = submitter.login_data

    assert result == {
            '_csrf_token': '666',
            '_username': 'username',
            '_password': 'password' }


def test_Submitter_csrf_token(submitter, mocker):
    submitter.source = b'html stuff [_token]" value="777" more html stuff'

    assert submitter.csrf_token == '777'


def test_parse_forum_id():
    with resources.open_text(testdata, 'forum-submit-sample.html') as resource:
        source = resource.read()

    assert parse_forum_id('meta', source) == '15'

    assert parse_forum_id('askraddle', source) == '60'


def test_Submitter_forum_id(submitter, mocker):
    submitter.source = 'html source'

    parser = mocker.patch('frogfeed.parse_forum_id', return_value='666')

    result = submitter.forum_id

    parser.assert_called_with('forum', 'html source')
    assert  result == '666'


def test_Submitter_make_submission_data(submitter, mocker):
    submitter.source = b'html stuff [_token]" value="777" more html stuff'
    mocker.patch('frogfeed.parse_forum_id', return_value='666')

    post = RedditPost('user', 'link', 'title', 'url', '2018-01-01T22:22+00:00')

    result = submitter.make_submission_data(post)

    assert result == {
            'submission[url]': 'link',
            'submission[title]': 'title',
            'submission[body]': 'author: user\n\nurl: url',
            'submission[forum]': '666',
            'submission[submit]': None,
            'submission[email]': None,
            'submission[_token]': '777'
            }


def test_HttpSubmitter_login(mocker):
    response = mocker.Mock()
    response.content = b'html stuff [_token]" value="777" more html stuff'
    response.cookies = 'cookiez'
    response.status_code = 302
    get_mock = mocker.patch('frogfeed.requests.get', return_value=response)
    post_mock = mocker.patch('frogfeed.requests.post', return_value=response)

    submitter = HttpSubmitter('username', 'password', 'forum')

    source = submitter.login()

    assert submitter.source.startswith(b'html stuff')
    assert submitter.cookies == 'cookiez'

    get_mock.assert_called_with(URL_LOGIN, allow_redirects=False, cookies=[])
    post_mock.assert_called_with(
        URL_LOGIN_CHECK,
        {'_csrf_token': '777',
         '_username': 'username',
         '_password': 'password' },
        cookies=response.cookies,
        allow_redirects=False
        )


def test_HttpSubmitter_submit(mocker):
    response = mocker.Mock()
    response.content = 'html stuff [_token]" value="888" more html stuff'
    response.cookies = 'cookiez'
    response.status_code = 302
    get_mock = mocker.patch('frogfeed.requests.get', return_value=response)
    post_mock = mocker.patch('frogfeed.requests.post', return_value=response)
    make_submission_data = mocker.patch.object(HttpSubmitter,
                                               'make_submission_data',
                                               return_value='data')

    submitter = HttpSubmitter('username', 'password', 'forum')

    source = submitter.submit(['post0', 'post1'])

    assert submitter.source.startswith('html stuff')
    assert submitter.cookies == 'cookiez'

    get_mock.assert_called_with(URL_FORUM_SUBMIT, allow_redirects=False, cookies='cookiez')
    post_mock.assert_called_with(
        URL_SUBMIT,
        'data',
        cookies=response.cookies,
        allow_redirects=False
        )
    assert make_submission_data.call_args_list == [
            call('post0'),
            call('post1')
            ]
